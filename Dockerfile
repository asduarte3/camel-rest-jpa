FROM openjdk:8-jdk-alpine

MAINTAINER alexandre

ENV TZ America/Sao_Paulo

COPY ./target/camel-rest-jpa.jar /app/

ENTRYPOINT exec java $JAVA_OPTIONS -jar /app/camel-rest-jpa.jar

EXPOSE 8080
