## Example using Spring Boot and Apache Camel
Product and Stock movements

### Requirements
Java 8

Apache Maven 3

Docker
### Run tests:
mvn test

### Run application:
mvn spring-boot:run

### Rest example URL:
http://localhost:8080/camelrestjpa/products/1

### Swagger UI:
http://localhost:8080/swagger-ui

### H2 console:
URL: http://localhost:8080/camelrestjpa/h2-console

JDBC URL: jdbc:h2:mem:camelrestjpadb
