DROP TABLE IF EXISTS stock_movement;
DROP TABLE IF EXISTS product;

CREATE TABLE product (
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  code VARCHAR(100) NOT NULL UNIQUE,
  description VARCHAR(250) NOT NULL,
  product_type VARCHAR(100) NOT NULL,
  price DECIMAL(10,2) NOT NULL,
  quantity INT NOT NULL,
  active INT NOT NULL
);

CREATE TABLE stock_movement (
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  product_id BIGINT NOT NULL,
  mov_type VARCHAR(100) NOT NULL,
  mov_date TIMESTAMP NOT NULL,
  price DECIMAL(20,2) NOT NULL,
  quantity INT NOT NULL,
  foreign key (product_id) references product(id)
);