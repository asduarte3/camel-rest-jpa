package com.alexandre.camelrestjpa.repository;

import com.alexandre.camelrestjpa.domain.StockMovement;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface StockMovementRepository extends CrudRepository<StockMovement, Long> {
    @Query("SELECT s FROM StockMovement s join fetch s.product p WHERE s.id = :id and p.active is true")
    Optional<StockMovement> findById(Long id);
}
