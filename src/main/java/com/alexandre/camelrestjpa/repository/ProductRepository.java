package com.alexandre.camelrestjpa.repository;

import com.alexandre.camelrestjpa.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {
    Page<Product> findByActiveTrue(Pageable pageable);

    Optional<Product> findByCodeAndActiveTrue(String code);

    Optional<Product> findByIdAndActiveTrue(Long id);

    boolean existsByIdAndActiveTrue(Long id);
}
