package com.alexandre.camelrestjpa.util;

import com.alexandre.camelrestjpa.dto.PageDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ModelMapperUtil {
    @Autowired
    private ModelMapper modelMapper;

    public <T> T map(Object source, Class<T> destinationType) {
        return modelMapper.map(source, destinationType);
    }

    public <S, T> PageDTO<T> mapPage(Page<S> page, Class<T> contentClass) {
        PageDTO<T> pageDTO = modelMapper.map(page, PageDTO.class);
        if (!CollectionUtils.isEmpty(page.getContent())) {
            pageDTO.setDataList(mapList(page.getContent(), contentClass));
        }
        return pageDTO;
    }

    public <S, T> List<T> mapList(List<S> source, Class<T> targetClass) {
        return source
                .stream()
                .map(element -> modelMapper.map(element, targetClass))
                .collect(Collectors.toList());
    }
}
