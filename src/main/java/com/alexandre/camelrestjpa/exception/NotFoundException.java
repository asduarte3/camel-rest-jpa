package com.alexandre.camelrestjpa.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends RestException {
    public NotFoundException(String entity) {
        super(entity + " not found", HttpStatus.NOT_FOUND);
    }
}
