package com.alexandre.camelrestjpa.exception;

import org.apache.camel.builder.RouteBuilder;

public class RestBaseRouteBuilder extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        onException(Throwable.class)
                .handled(true)
                .to("bean:restExceptionHandler?method=errorHandling(${exchange})");
    }
}
