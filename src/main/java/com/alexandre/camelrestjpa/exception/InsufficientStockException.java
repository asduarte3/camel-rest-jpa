package com.alexandre.camelrestjpa.exception;

import org.springframework.http.HttpStatus;

public class InsufficientStockException extends RestException {
    public InsufficientStockException() {
        super("Insufficient stock", HttpStatus.BAD_REQUEST);
    }
}
