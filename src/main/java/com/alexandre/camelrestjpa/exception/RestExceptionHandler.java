package com.alexandre.camelrestjpa.exception;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Slf4j
@Component
public class RestExceptionHandler {

    public ExceptionResponse errorHandling(Exchange exchange) {
        Throwable t = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Throwable.class);

        ExceptionResponse exceptionResponse = new ExceptionResponse();
        exceptionResponse.setErrorMessage(t.getMessage());
        exceptionResponse.setTimestamp(LocalDateTime.now());

        HttpStatus status = HttpStatus.BAD_REQUEST;
        if (t instanceof RestException) {
            RestException restException = (RestException) t;
            status = restException.getStatus();
        } else {
            log.error("", t);
        }

        exchange.getIn().setHeader(Exchange.HTTP_RESPONSE_CODE, status.value());

        return exceptionResponse;
    }
}
