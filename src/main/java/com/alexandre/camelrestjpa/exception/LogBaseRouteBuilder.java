package com.alexandre.camelrestjpa.exception;

import org.apache.camel.builder.RouteBuilder;

public class LogBaseRouteBuilder extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        onException(Throwable.class)
                .handled(true)
                .log("Excpetion caught: ${exception.stacktrace}");
    }
}
