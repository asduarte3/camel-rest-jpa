package com.alexandre.camelrestjpa.service;

import com.alexandre.camelrestjpa.domain.Product;
import com.alexandre.camelrestjpa.dto.IdDTO;
import com.alexandre.camelrestjpa.dto.PageDTO;
import com.alexandre.camelrestjpa.dto.PageRequestDTO;
import com.alexandre.camelrestjpa.dto.ProductDTO;
import com.alexandre.camelrestjpa.exception.NotFoundException;
import com.alexandre.camelrestjpa.repository.ProductRepository;
import com.alexandre.camelrestjpa.util.ModelMapperUtil;
import org.apache.camel.Header;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository repository;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Transactional(rollbackFor = Throwable.class)
    public Product create(Product product) {
        product.setId(null);
        product.setActive(true);
        return save(product);
    }

    @Transactional(rollbackFor = Throwable.class)
    public Product save(Product product) {
        return repository.save(product);
    }

    @Transactional(rollbackFor = Throwable.class)
    public Product update(ProductDTO dto) {
        boolean found = existsById(dto.getId());
        if (found) {
            Product product = modelMapperUtil.map(dto, Product.class);
            return save(product);
        } else {
            throw new NotFoundException("Product");
        }
    }

    public Page<Product> findAll(Pageable pageable) {
        return repository.findByActiveTrue(pageable);
    }

    private Product findById(Long id) {
        Optional<Product> product = repository.findById(id);
        return getProduct(product);
    }

    private Product getProduct(Optional<Product> product) {
        if (product.isPresent()) {
            return product.get();
        } else {
            throw new NotFoundException("Product");
        }
    }

    public Product findByIdActive(Long id) {
        Optional<Product> product = repository.findByIdAndActiveTrue(id);
        return getProduct(product);
    }

    public Product findByCode(String code) {
        Optional<Product> product = repository.findByCodeAndActiveTrue(code);
        return getProduct(product);
    }

    private boolean existsById(Long id) {
        return repository.existsByIdAndActiveTrue(id);
    }

    @Transactional(rollbackFor = Throwable.class)
    public void inactivateById(Long id) {
        Product product = findById(id);
        product.setActive(false);
        save(product);
    }

    public ProductDTO toDTO(Product product) {
        return modelMapperUtil.map(product, ProductDTO.class);
    }

    public Product toEntity(ProductDTO dto) {
        return modelMapperUtil.map(dto, Product.class);
    }

    public IdDTO toIdDTO(Product product) {
        return modelMapperUtil.map(product, IdDTO.class);
    }

    public PageDTO<ProductDTO> toPageDTO(Page<Product> page) {
        return modelMapperUtil.mapPage(page, ProductDTO.class);
    }

    public PageRequest getPageRequest(@Header("page") int page, @Header("size") int size) {
        PageRequestDTO pageRequestDTO = new PageRequestDTO(page, size);
        return pageRequestDTO.getPageRequest(Sort.Direction.ASC, "code");
    }
}
