package com.alexandre.camelrestjpa.service;

import com.alexandre.camelrestjpa.domain.MovementTypeEnum;
import com.alexandre.camelrestjpa.domain.Product;
import com.alexandre.camelrestjpa.domain.StockMovement;
import com.alexandre.camelrestjpa.dto.IdDTO;
import com.alexandre.camelrestjpa.dto.StockMovementDTO;
import com.alexandre.camelrestjpa.exception.InsufficientStockException;
import com.alexandre.camelrestjpa.exception.NotFoundException;
import com.alexandre.camelrestjpa.repository.StockMovementRepository;
import com.alexandre.camelrestjpa.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.RabbitMQContainer;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class StockMovementService {

    @Autowired
    private StockMovementRepository repository;

    @Autowired
    private ProductService productService;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Autowired
    private RabbitMQContainer rabbitMQContainer;

    public StockMovement findById(Long id) {
        Optional<StockMovement> stockMovement = repository.findById(id);
        if (stockMovement.isPresent()) {
            return stockMovement.get();
        } else {
            throw new NotFoundException("Stock Movement");
        }
    }

    @Transactional(rollbackFor = Throwable.class)
    public StockMovement create(StockMovement stockMovement) {
        Product product = saveProduct(stockMovement);
        stockMovement.setId(null);
        stockMovement.setProduct(product);
        stockMovement.setMovementDate(LocalDateTime.now());
        return repository.save(stockMovement);
    }

    private Product saveProduct(StockMovement stockMovement) {
        Product product = productService.findByIdActive(stockMovement.getProduct().getId());
        int quantidade = MovementTypeEnum.OUT.equals(stockMovement.getMovementType()) ? -(stockMovement.getQuantity()) : stockMovement.getQuantity();
        product.setQuantity(product.getQuantity() + quantidade);
        if (product.getQuantity() >= 0) {
            return productService.save(product);
        } else {
            throw new InsufficientStockException();
        }
    }

    public StockMovementDTO createRandom() {
        System.out.println(rabbitMQContainer.getMappedPort(5672));

        return StockMovementDTO.builder()
                .product(new IdDTO(getRandomNumber(1, 10).longValue()))
                .movementType(MovementTypeEnum.values()[getRandomNumber(0, 1)])
                .quantity(getRandomNumber(1, 5))
                .price(getRandomNumber(1, 100).doubleValue())
                .build();
    }

    private Integer getRandomNumber(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    public StockMovementDTO toDTO(StockMovement stockMovement) {
        return modelMapperUtil.map(stockMovement, StockMovementDTO.class);
    }

    public StockMovement toEntity(StockMovementDTO dto) {
        return modelMapperUtil.map(dto, StockMovement.class);
    }

    public IdDTO toIdDTO(StockMovement stockMovement) {
        return modelMapperUtil.map(stockMovement, IdDTO.class);
    }
}


