package com.alexandre.camelrestjpa.dto;

import com.alexandre.camelrestjpa.domain.ProductTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDTO {
    private Long id;

    private String code;

    private String description;

    private ProductTypeEnum productType;

    private Double price;

    private Integer quantity;

    private Boolean active;
}
