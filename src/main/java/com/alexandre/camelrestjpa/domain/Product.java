package com.alexandre.camelrestjpa.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "code", "description"})
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 100)
    private String code;

    @NotBlank
    @Size(max = 250)
    private String description;

    @Enumerated(EnumType.STRING)
    @NotNull
    private ProductTypeEnum productType;

    @DecimalMin(value = "0.0", inclusive = false)
    @Digits(integer = 10, fraction = 2)
    private Double price;

    @Min(value = 0)
    @Max(value = 9999)
    private Integer quantity;

    private Boolean active = true;
}
