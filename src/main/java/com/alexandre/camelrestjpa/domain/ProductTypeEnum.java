package com.alexandre.camelrestjpa.domain;

public enum ProductTypeEnum {
    ELETRONIC,
    DOMESTIC_APPLIANCES,
    FURNITURE
}
