package com.alexandre.camelrestjpa.domain;

public enum MovementTypeEnum {
    IN,
    OUT
}
