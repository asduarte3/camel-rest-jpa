package com.alexandre.camelrestjpa.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id"})
@Entity
public class StockMovement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @Column(name = "mov_type")
    @Enumerated(EnumType.STRING)
    @NotNull
    private MovementTypeEnum movementType;

    @Column(name = "mov_date")
    @NotNull
    private LocalDateTime movementDate;

    @DecimalMin(value = "0.0", inclusive = false)
    @Digits(integer = 10, fraction = 2)
    private Double price;

    @Min(value = 1)
    @Max(value = 9999)
    private Integer quantity;
}
