package com.alexandre.camelrestjpa.router;

import com.alexandre.camelrestjpa.dto.IdDTO;
import com.alexandre.camelrestjpa.dto.StockMovementDTO;
import com.alexandre.camelrestjpa.exception.RestBaseRouteBuilder;
import com.alexandre.camelrestjpa.service.StockMovementService;
import org.apache.camel.Exchange;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import static org.apache.camel.model.rest.RestParamType.path;

@Component
public class StockMovementRouter extends RestBaseRouteBuilder {

    @Override
    public void configure() throws Exception {
        super.configure();
        rest("/stockmovements").description("Stock movements REST service")
                .consumes("application/json")
                .produces("application/json")

                .get("/{id}").description("Find stock movement by ID")
                .outType(StockMovementDTO.class)
                .param().name("id").type(path).description("The Stock movement ID").dataType("integer").required(true).endParam()
                .route()
                .to("bean:stockMovementService?method=findById(${header.id})")
                .to("bean:stockMovementService?method=toDTO")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpStatus.OK.value()))
                .endRest()

                .post().description("Stock movement create")
                .type(StockMovementDTO.class)
                .outType(IdDTO.class)
                .route()
                .bean(StockMovementService.class, "toEntity")
                .bean(StockMovementService.class, "create")
                .bean(StockMovementService.class, "toIdDTO")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpStatus.CREATED.value()))
                .endRest();
    }
}
