package com.alexandre.camelrestjpa.router;

import com.alexandre.camelrestjpa.dto.IdDTO;
import com.alexandre.camelrestjpa.dto.PageDTO;
import com.alexandre.camelrestjpa.dto.ProductDTO;
import com.alexandre.camelrestjpa.exception.RestBaseRouteBuilder;
import com.alexandre.camelrestjpa.service.ProductService;
import org.apache.camel.Exchange;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import static org.apache.camel.model.rest.RestParamType.path;
import static org.apache.camel.model.rest.RestParamType.query;

@Component
public class ProductRouter extends RestBaseRouteBuilder {

    @Override
    public void configure() throws Exception {
        super.configure();
        rest("/products").description("Products REST service")
                .consumes("application/json")
                .produces("application/json")

                .get().description("Find all product")
                .param().name("page").type(query).description("Page (zero based)").defaultValue("0").dataType("integer").required(true).endParam()
                .param().name("size").type(query).description("Size").defaultValue("10").dataType("integer").required(true).endParam()
                .outType(PageDTO.class)
                .route()
                .bean(ProductService.class, "getPageRequest")
                .bean(ProductService.class, "findAll")
                .bean(ProductService.class, "toPageDTO")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpStatus.OK.value()))
                .endRest()

                .get("/{id}").description("Find product by ID")
                .param().name("id").type(path).description("The product ID").dataType("integer").required(true).endParam()
                .outType(ProductDTO.class)
                .route()
                .to("bean:productService?method=findByIdActive(${header.id})")
                .to("bean:productService?method=toDTO")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpStatus.OK.value()))
                .endRest()

                .get("/code/{code}").description("Find product by ID")
                .param().name("code").type(path).description("The product CODE").dataType("string").required(true).endParam()
                .outType(ProductDTO.class)
                .route()
                .to("bean:productService?method=findByCode(${header.code})")
                .to("bean:productService?method=toDTO")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpStatus.OK.value()))
                .endRest()

                .post().description("Product create")
                .type(ProductDTO.class)
                .outType(IdDTO.class)
                .route()
                .bean(ProductService.class, "toEntity")
                .bean(ProductService.class, "create")
                .bean(ProductService.class, "toIdDTO")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpStatus.CREATED.value()))
                .endRest()

                .put().description("Product update")
                .type(ProductDTO.class)
                .outType(IdDTO.class)
                .route()
                .bean(ProductService.class, "update")
                .bean(ProductService.class, "toIdDTO")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpStatus.OK.value()))
                .endRest()

                .delete("/{id}").description("Delete product by ID")
                .param().name("id").type(path).description("The product ID").dataType("integer").required(true).endParam()
                .route()
                .to("bean:productService?method=inactivateById(${header.id})")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpStatus.NO_CONTENT.value()))
                .endRest();

    }
}
