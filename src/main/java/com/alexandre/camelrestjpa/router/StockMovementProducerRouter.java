package com.alexandre.camelrestjpa.router;

import com.alexandre.camelrestjpa.exception.LogBaseRouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class StockMovementProducerRouter extends LogBaseRouteBuilder {

    @Override
    public void configure() throws Exception {
        super.configure();
        from("timer://random-stock?delay=60000&fixedRate=true&period=10000")
                .to("bean:stockMovementService?method=createRandom")
                .to("rabbitmq:create-random-stock?routingKey=stock")
                .log("Movement Stock sent to queue: ${body}");
    }
}
