package com.alexandre.camelrestjpa.router;

import com.alexandre.camelrestjpa.exception.LogBaseRouteBuilder;
import com.alexandre.camelrestjpa.service.StockMovementService;
import org.springframework.stereotype.Component;

@Component
public class StockMovementConsumerRouter extends LogBaseRouteBuilder {

    @Override
    public void configure() throws Exception {
        super.configure();
        from("rabbitmq:create-random-stock?routingKey=stock")
                .bean(StockMovementService.class, "toEntity")
                .bean(StockMovementService.class, "create")
                .log("Movement Stock created: ${body}");
    }
}
