package com.alexandre.camelrestjpa.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public
class SwaggerController {
    @Value("${camel.rest.context-path}")
    private String camelContextPath;

    @Value("${camel.rest.api-context-path}")
    private String camelApiContextPath;

    @RequestMapping("/swagger-ui")
    public String redirectToUi() {
        return "redirect:/webjars/swagger-ui/index.html?url=" + camelContextPath + camelApiContextPath +
                "&validatorUrl=";
    }
}
