package com.alexandre.camelrestjpa.config;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.testcontainers.containers.RabbitMQContainer;

@Configuration
public class Config {
    @Value("${camel.component.rabbitmq.hostname}")
    private String rabbitmqHost;

    @Value("${camel.component.rabbitmq.port-number}")
    private String rabbitmqPort;

    @Value("${camel.component.rabbitmq.username}")
    private String rabbitmqUsername;

    @Value("${camel.component.rabbitmq.password}")
    private String rabbitmqPassword;

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public RabbitMQContainer rabbitMQContainer() {
        RabbitMQContainer rabbitMQContainer = new RabbitMQContainer("rabbitmq:3-management");
        rabbitMQContainer.start();
        return rabbitMQContainer;
    }
}
