package com.alexandre.camelrestjpa.camelrestjpa;

import com.alexandre.camelrestjpa.domain.Product;
import com.alexandre.camelrestjpa.domain.ProductTypeEnum;
import com.alexandre.camelrestjpa.dto.ProductDTO;
import com.alexandre.camelrestjpa.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
class ProductHelper {
    @Autowired
    private ProductService service;

    public Product create() {
        String code = String.valueOf(System.currentTimeMillis());
        Product product = Product.builder()
                .code(code)
                .description("product " + code)
                .productType(ProductTypeEnum.ELETRONIC)
                .quantity(10)
                .price(100d)
                .build();
        product = service.create(product);
        return product;
    }

    public ProductDTO createDTO() {
        String code = String.valueOf(System.currentTimeMillis());
        ProductDTO dto = ProductDTO.builder()
                .code(code)
                .description("product " + code)
                .productType(ProductTypeEnum.ELETRONIC)
                .quantity(10)
                .price(100d)
                .build();
        return dto;
    }
}