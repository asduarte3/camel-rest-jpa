package com.alexandre.camelrestjpa.camelrestjpa;

import com.alexandre.camelrestjpa.domain.Product;
import com.alexandre.camelrestjpa.dto.IdDTO;
import com.alexandre.camelrestjpa.dto.PageDTO;
import com.alexandre.camelrestjpa.dto.ProductDTO;
import com.alexandre.camelrestjpa.util.ModelMapperUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.camel.test.spring.junit5.CamelSpringBootTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@CamelSpringBootTest
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class ProductTest {
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProductHelper helper;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Autowired
    private TestRestTemplate restTemplate;

    @Value("${camel.rest.context-path}")
    private String camelContextPath;

    @Test
    void testFindAll() throws Exception {
        ResponseEntity<PageDTO> response = restTemplate.getForEntity(camelContextPath + "/products?page=0&size=10", PageDTO.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        PageDTO page = response.getBody();
        assertThat(page.getDataList().size()).isPositive();
    }

    @Test
    void testFindById() throws Exception {
        Product product = helper.create();
        ResponseEntity<ProductDTO> response = restTemplate.getForEntity(camelContextPath + "/products/" + product.getId(), ProductDTO.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        ProductDTO dto = response.getBody();
        assertThat(dto.getId()).isEqualTo(product.getId());
    }

    @Test
    void testFindByCode() throws Exception {
        Product product = helper.create();
        ResponseEntity<ProductDTO> response = restTemplate.getForEntity(camelContextPath + "/products/code/" + product.getCode(), ProductDTO.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        ProductDTO dto = response.getBody();
        assertThat(dto.getId()).isEqualTo(product.getId());
    }

    @Test
    void testCreate() throws Exception {
        ProductDTO dto = helper.createDTO();
        ResponseEntity<IdDTO> response = restTemplate.postForEntity(camelContextPath + "/products", dto, IdDTO.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    void testUpdate() throws Exception {
        Product product = helper.create();
        ProductDTO dto = modelMapperUtil.map(product, ProductDTO.class);
        dto.setQuantity(dto.getQuantity() + 1);
        ResponseEntity<IdDTO> response = restTemplate.exchange(camelContextPath + "/products",
                HttpMethod.PUT, new HttpEntity<ProductDTO>(dto), IdDTO.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void testDelete() throws Exception {
        Product product = helper.create();
        ResponseEntity<Optional> response = restTemplate.exchange(camelContextPath + "/products/" + product.getId(),
                HttpMethod.DELETE, null, Optional.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

}