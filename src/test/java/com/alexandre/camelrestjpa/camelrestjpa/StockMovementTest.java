package com.alexandre.camelrestjpa.camelrestjpa;

import com.alexandre.camelrestjpa.domain.MovementTypeEnum;
import com.alexandre.camelrestjpa.domain.Product;
import com.alexandre.camelrestjpa.dto.IdDTO;
import com.alexandre.camelrestjpa.dto.StockMovementDTO;
import com.alexandre.camelrestjpa.util.ModelMapperUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.camel.test.spring.junit5.CamelSpringBootTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

@CamelSpringBootTest
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class StockMovementTest {
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProductHelper helper;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Autowired
    private TestRestTemplate restTemplate;

    @Value("${camel.rest.context-path}")
    private String camelContextPath;

    @Test
    void testCreate() throws Exception {
        Product product = helper.create();
        StockMovementDTO dto = StockMovementDTO.builder()
                .product(new IdDTO(product.getId()))
                .movementType(MovementTypeEnum.IN)
                .quantity(5)
                .price(100d)
                .build();

        ResponseEntity<IdDTO> response = restTemplate.postForEntity(camelContextPath + "/stockmovements", dto, IdDTO.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }
}